package game;

import javafx.scene.image.Image;

public class Imagesload {
    private  String status;

    /**
     * Contructor ti get condition from scene
     * @param status season
     */
    public Imagesload(String status){
        if(status==null){
            this.status = "summer";
        }
        else {
            this.status = status;
        }

    }

    /**
     * Set image for ground if conditions of seasons are met
     * @return specific image
     */
    public Image setImageGround(){
        Image image;

        if (status.equals("winter")){
            image = new Image("images/17427486.jpg");
        }else if(status.equals("summer")){
            image = new Image("images/ground.png");
        }
        else {
            image = new Image("images/ground.png");
        }
        return image;
    }

    /**
     * Set image for wall if conditions of seasons are met
     * @return specific image
     */
    public Image seImageWall(){
        Image image;
        if (status.equals("winter")){
            image = new Image("images/pine-tree-png-clip-art.png");
        }else if(status.equals("summer")){
            image = new Image("images/hedge.png");
        }
        else {
            image = new Image("images/hedge.png");
        }
        return image;

    }

    /**
     * Set image for rain if conditions of seasons are met
     * @return specific image
     */
    public Image setImageRain(){
        Image image;
        if (status.equals("winter")){
            image = new Image("images/1458424-snowing-pixel-art (1).png");
        }else if(status.equals("summer")){
            image = new Image("images/rain.png");
        }
        else {
            image = new Image("images/rain.png");
        }
        return image;

    }
    }


