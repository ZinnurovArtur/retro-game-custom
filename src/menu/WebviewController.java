package menu;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.WindowEvent;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class represents full implementation of audio player program
 * @author Arthur Zinnurov
 * @version 1.2
 */

public class WebviewController {
    @FXML
    private Button pause;
    @FXML
    private Button play;
    @FXML
    private Button next;
    @FXML
    private TextField searchField;
    @FXML
    private WebView webView;
    @FXML
    private Label title;
    @FXML
    private ImageView view;
    private Robot simulatorKeys;




    /**
     * Finds a track from Youtube using webview
     *
     */
    public void findMusic() {
        if (!searchField.getText().trim().isEmpty()) {
            System.out.println(searchField.getText());
            WebEngine engine = webView.getEngine();
            // engine.load("https://www.youtube.com/");
            //webView.setPrefSize(200, 200);
            // engine.load("https://www.youtube.com/results?search_query="+searchField.getText());
            engine.load("https://www.youtube.com" + loadMusic(searchField.getText()));
            System.out.println(loadMusic(searchField.getText()));
        } else {
            searchField.setPromptText("Failed! Your line is empty");

        }
    }

    /**
     * Backend implementation of finding track  by url link
     * @param query
     * @return string url
     */
    public String loadMusic(String query) {
        String all = "";

        try {
            String url = "http://www.youtube.com/results";

            Document doc = Jsoup.connect(url)
                    .data("search_query", query)
                    .userAgent("Mozilla/5.0")
                    .get();
            Element videoUrl = doc.select(".yt-lockup-title > a[title]").first();
            String idandurl = videoUrl.attr("href");
            all = idandurl;
            title.setText("Now Playing: " + videoUrl.attr("title"));
            String forimage = idandurl.substring(9);
            view.setImage(new Image("https://i.ytimg.com/vi/" + forimage + "/mqdefault.jpg"));

            System.out.println(forimage);


        } catch (IOException e) {
            e.printStackTrace();
        }
        return all;

    }

    /**
     * Go to next track if button next is clicked
     */
    public void nextTrek() {
        try {
            webView.requestFocus();
            simulatorKeys = new Robot();
            simulatorKeys.keyPress(KeyEvent.VK_SHIFT);
            simulatorKeys.keyPress(KeyEvent.VK_N);
            simulatorKeys.delay(10);
            simulatorKeys.keyRelease(KeyEvent.VK_N);
            simulatorKeys.keyRelease(KeyEvent.VK_SHIFT);

            webView.getEngine().reload();
            webView.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
                @Override
                public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) {
                    if (newValue == Worker.State.SUCCEEDED) {
                        title.setText("Now Playing: " + webView.getEngine().getTitle());
                        String fullurl = webView.getEngine().getLocation();
                        String idnaurl = webView.getEngine().getLocation().substring(32, fullurl.indexOf("&"));
                        view.setImage(new Image("https://i.ytimg.com/vi/" + idnaurl + "/mqdefault.jpg"));
                    }
                }
            });

        } catch (AWTException e) {
            e.printStackTrace();
        }

    }

    /**
     * Pause track if button pause is clicked
     */
    public void pauseTrek() {
        try {
            webView.requestFocus();
            simulatorKeys = new Robot();
            simulatorKeys.keyPress(KeyEvent.VK_SPACE);
            simulatorKeys.delay(10);
            simulatorKeys.keyRelease(KeyEvent.VK_SPACE);
        }catch (AWTException e){
            e.printStackTrace();
        }
    }









}
