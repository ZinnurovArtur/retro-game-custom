package menu;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * This class will contain the instructions for the buttons and combo boxes of the LoadMaps class
 * @author Arthur Zinnurov
 * @author Suad Ahmadieh Mena
 */
public class LoadMapsController implements Initializable {
    private PlayButton buttonSound = new PlayButton(60);


    @FXML
    private ComboBox<String> comboBox;

    private String mapName;

    static String nameTopass;

    @FXML
    private Button loadButton;
    private File[] combined;






    /**
     * When you click the load button, the scene will change to the one where you choose between going to CustomMaps,
     * System maps or level editor.
     *
     * @param event As a user clicks a button, presses a key, moves a mouse, or performs other actions, events
     *              are dispatched.
     * @throws IOException whenever the FXML file LevelEditor is not working or existing.
     */
    public void loadButton(ActionEvent event) throws IOException {
        buttonSound.playButton();
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("CustomMap.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        // This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();

    }

    /**
     * Delete button with action
     * @param event
     * @throws IOException
     */
    public void deleteButton(ActionEvent event) throws IOException{
        mapTodelete();

    }



    /**
     * Initialise the canvas and start handling the Combo boxes.
     *
     * @param location  Location to init from
     * @param resources Resources to load
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        GetFileList listMaps = new GetFileList();
        File[] fileList = listMaps.finderMap("src/custom_maps");
        File[] filesList2 = listMaps.finderMap("src/maps");
        combined = combineArr(fileList,filesList2);
        System.out.println(Arrays.toString(combined));
        for (int i = 0; i < combined.length; i++) {
            comboBox.getItems().add(combined[i].getName());
        }
        comboBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> setSelectedMap());

    }


    /**
     * Set the name of the map from Combobox value event handler.
     */
    private void setSelectedMap() {
        this.mapName = comboBox.getValue();
        for (int i = 0;i<combined.length;i++){
            if (combined[i].getName().equals(this.mapName)){
                nameTopass = combined[i].getAbsolutePath();
                System.out.println(nameTopass);
            }
        }
    }

    /**
     * Combine two file arrays
     * @param a first file array
     * @param b second file array
     * @return combined file array
     */
    private File[] combineArr(File[] a, File[] b){

        int length = a.length + b.length;
        File[] combined = new File[length];
        System.arraycopy(a,0,combined,0,a.length);
        System.arraycopy(b,0,combined,a.length,b.length);
        return combined;
    }

    /**
     * Delete specific map files selected by combobox
     */
    private void mapTodelete(){
        GetFileList listMaps = new GetFileList();
        File[] fileList = listMaps.finderMap("src/custom_maps");
        for (int i = 0; i<fileList.length; i++){
            if (fileList[i].getName().equals(this.mapName)){
                File filetodel = new File(System.getProperty("user.dir")+
                        File.separator +"src"+File.separator + "custom_maps" + File.separator + this.mapName);
                if(filetodel.delete()){
                    System.out.println("Custom file deleted");
                    comboBox.getItems().remove(this.mapName);
                }
            }
        }


    }

    /**
     * Full path to pass to the next scene
     * @return string path
     */
    public String getNameTopass(){
        return nameTopass;
    }


}
