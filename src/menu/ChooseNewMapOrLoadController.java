package menu;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * This class will control the buttons of the ChooseNewMapOrReloadMap where the user can choose between
 * creating a new map or reloading an already existing map
 * @author Arthur Zinnurov
 * @author Suad Ahmadieh Mena
 */
public class ChooseNewMapOrLoadController {
    private static boolean isToload;

    /**
     * Method to change scenes from  to start creating or reloading profiles when you click the Play Button.
     * @param event Event of the user clicking the button
     * @throws IOException when the CustomMap FXML is not loading or does not exist.
     */
    @FXML
    public void createANewMap(ActionEvent event) throws IOException {
        isToload = false;
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("size.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        // This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
       // window.close();
    }

    /**
     * Method to change scenes from play game to start creating or reloading profiles when you click the Play Button.
     * @param event Event of the user clicking the button
     * @throws IOException when the CustomMap FXML is not loading or does not exist.
     */
    @FXML
    public void reloadAnExistingMap(ActionEvent event) throws IOException {
        isToload = true;
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("LoadMaps.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        // This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }

    /**
     * Method to check which option user has choosen reload map or create new
     * @return check if next option either will be for load or to create new map
     */
    public Boolean istoReload(){
        return isToload;
    }


}
