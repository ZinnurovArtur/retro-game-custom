package menu;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 * This class initialise the whole level editor using a new Stage created
 * @author Arthur Zinnurov
 * @version  1.1
 */
public class MainEditor extends Application {

    /**
     * Initialise the main level editor creating a new stage called Editor
     * @param primaryStage First stage of the level editor
     * @throws Exception When the file size cannot be loaded or used
     */
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("size.fxml"));

        Scene scene = new Scene(root);

        primaryStage.setTitle("Editor");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Main method to start and display the whole menu.
     * @param args Commandline arguments
     */
    public static void main(String[] args){
        launch(args);
    }
}
