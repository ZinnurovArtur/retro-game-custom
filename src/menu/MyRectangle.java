package menu;

import javafx.scene.shape.Rectangle;


/**
 * Class to specify additional information of each grid
 * @author Piotr Obara
 */

public class MyRectangle extends Rectangle {

    private String name;
    private int requiredTokenDoor = 0;
    private String directionEnemy = "";


    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }

    /**
     * Set the required number of tokens to open the door to the specific grid
     * @param requiredTokenDoor  from the stage
     */
    public void setRequiredTokenDoor(int requiredTokenDoor){
        this.requiredTokenDoor = requiredTokenDoor;
    }

    /**
     * Get the required number of tokens to open the door of the specific grid
     * @return int tokens
     */
    public int getRequiredTokenDoor(){
        return this.requiredTokenDoor;
    }

    /**
     * Set the name direction of the Enemy from stage
     * @param directionEnemy
     */
    public void setDirectionEnemy(String directionEnemy){
        this.directionEnemy = directionEnemy;
    }

    /**
     * Get the Direction name in order to save it in map file
     * @return string direction
     */
    public String getDirectionEnemy(){
        return this.directionEnemy;
    }

}
