package menu;

import game.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * This class is the most important class for the level editor which includes all the information related to the
 * different combo boxes, the pane of grids and how to connect each of the grids with
 * the element that the user wants to put in there and how to save the current created map as a file.
 *
 * @author Arthur Zinnurov
 * @author Suad Ahmadieh Mena
 * @author Piotr Obara
 * @version 1.0
 */
public class CustomMapsController implements Initializable {
    private FXMLLoader sizeController = new FXMLLoader(getClass().getResource("size.fxml"));
    private String data;
    private String[] cells = new String[]{"Ice", "Wind", "Rain", "BlueDoor", "RedDoor", "GreenDoor", "Ground", "Teleporter", "Wall", "Token", "Player", "Goal", "TokenDoor"};
    private String[] enemys = new String[]{"SmartTarget", "StraightLine", "DumbTarget", "WallFollow"};
    private String[] items = new String[]{"WindJacket", "RainUmbrella", "BlueKey", "RedKey", "GreenKey"};

    Node firstTeleporter = null;
    Node secondTeleporter = null;
    boolean border;


    @FXML
    private Button delete;
    @FXML
    private Button save;
    @FXML
    private ComboBox<String> comboBoxEnemies;
    @FXML
    private ComboBox<String> comboBoxItems;
    @FXML
    private ComboBox<String> comboBoxCells;
    @FXML
    private TilePane gridPane = new TilePane();
    @FXML
    private Slider sliderScale;
    private int columnCount;
    private int rowCount;


    /**
     * Initialise the canvas and start handling the Combo boxes.
     *
     * @param location  Location to init from
     * @param resources Resources to load
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            Parent loginRoot = this.sizeController.load();//Must not be deleted
            SizeController sizeController = this.sizeController.getController();
            ChooseNewMapOrLoadController contr = new ChooseNewMapOrLoadController();
            LoadMapsController loadcont = new LoadMapsController();
            int xCoordinate = sizeController.getxValue();
            int yCoordinate = sizeController.getyValue();
            //put your path to the testing map
            if (contr.istoReload()) {
                Map map = new Map(loadcont.getNameTopass());
                int x = map.getSizeX();
                int y = map.getSizeY();
                System.out.println(x + " " + y);
                if (x < y) {
                    x += (y - x);
                } else {
                    y += (x - y);
                }
                drawCustomGrid(x, y, loadcont.getNameTopass());
            } else {
                drawGrid(xCoordinate, yCoordinate);
            }
        } catch (IOException e) {
            System.exit(-1);
        }

        comboBoxCells.getItems().addAll(cells);
        comboBoxEnemies.getItems().addAll(enemys);
        comboBoxItems.getItems().addAll(items);

        comboBoxCells.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> setSelected());
        comboBoxEnemies.getSelectionModel().selectedIndexProperty().addListener((v, oldValue, newValue) -> setSelected());
        comboBoxItems.getSelectionModel().selectedIndexProperty().addListener((v, oldValue, newValue) -> setSelected());
        sliderScale.valueProperty().addListener((obs, oldValue, newValue) -> changeSizeDimension());

    }

    /**
     * Change the size of the grid by perfoming Slider
     */
    private void changeSizeDimension() {
        for (Node n : gridPane.getChildren()) {
            ((MyRectangle) n).setWidth(sliderScale.getValue());
            ((MyRectangle) n).setHeight(sliderScale.getValue());
        }
    }


    /**
     * Method to disable the other two combo Boxes whenever we are using one of the three.
     */
    private void setSelected() {
        if (comboBoxCells.isFocused()) {
            this.data = comboBoxCells.getValue();
            comboBoxEnemies.setDisable(false);
            comboBoxItems.setDisable(false);
        } else if (comboBoxEnemies.isFocused()) {
            this.data = comboBoxEnemies.getValue();
            comboBoxCells.setDisable(false);
            comboBoxItems.setDisable(false);
        } else if (comboBoxItems.isFocused()) {
            this.data = comboBoxItems.getValue();
            comboBoxCells.setDisable(false);
            comboBoxEnemies.setDisable(false);
        }
    }


    /**
     * Set the columns of the Grid of Panes and create the corresponding elements
     *
     * @param newColumns Columns of the pane of Grids
     */
    private void setColumns(int newColumns) {
        columnCount = newColumns;
        gridPane.setPrefColumns(columnCount);
        createElements();
    }

    /**
     * Method that goes by each grid of the pane and adds the corresponding element
     */
    private void createElements() {
        gridPane.getChildren().clear();
        for (int i = 0; i < columnCount; i++) {
            for (int j = 0; j < rowCount; j++) {
                gridPane.getChildren().add(createElement(i, j));
            }
        }
    }

    /**
     * Method to access any particular grid of the pane when the user clicks on any square of the pane
     */
    private void accessRectangle() {
        gridPane.setOnMouseEntered(event1 -> {
            for (Node n : gridPane.getChildren()) {

                n.setOnMouseClicked(event -> {
                    validateGrid((MyRectangle) n);
                });

            }
        });
    }

    /**
     * Method that will check how many teleporters and players are already existing in the pane of grids and will
     * not allow the user to add more than one player or more than two teleporters with the combo Boxes.
     *
     * @param myRectangle One of the squares that are in the grid of panes.
     */
    private void validateGrid(MyRectangle myRectangle) {
        border = true;
        System.out.println(myRectangle.getX() + " " + myRectangle.getY());

        if (!delete.isDisabled()) {
            if (data != null) {
                if (!data.equals("Teleporter") && myRectangle.getId().equals("Teleporter")) {
                    firstTeleporter.setId("id");
                    ((Rectangle) firstTeleporter).setFill(Color.WHITE);
                    secondTeleporter.setId("id");
                    ((Rectangle) secondTeleporter).setFill(Color.WHITE);
                }
                switch (data) {
                    case "Teleporter":
                        if (countID("Teleporter") == 0) {
                            myRectangle.setId("Teleporter");
                            myRectangle.setFill(new ImagePattern(new Image(getImage())));
                            firstTeleporter = myRectangle;
                            disableBoxes();
                        } else if (countID("Teleporter") == 1) {
                            if (firstTeleporter != myRectangle) {
                                myRectangle.setId("Teleporter");
                                myRectangle.setFill(new ImagePattern(new Image(getImage())));
                                secondTeleporter = myRectangle;
                                enableBoxes();
                            }
                        } else if (countID("Teleporter") > 1) {
                            if (firstTeleporter != myRectangle && secondTeleporter != myRectangle) {
                                firstTeleporter.setId("id");
                                ((Rectangle) firstTeleporter).setFill(Color.WHITE);
                                myRectangle.setId("Teleporter");
                                myRectangle.setFill(new ImagePattern(new Image(getImage())));
                                firstTeleporter = secondTeleporter;
                                secondTeleporter = myRectangle;
                            }
                        }
                        break;
                    case "Player":
                        if (countID("Player") == 0) {
                            myRectangle.setId("Player");
                            myRectangle.setFill(new ImagePattern(new Image(getImage())));
                        } else if (countID("Player") > 0) {
                            for (Node no : gridPane.getChildren()) {
                                if (no.getId().equals("Player")) {
                                    no.setId("id");
                                    ((Rectangle) no).setFill(Color.WHITE);
                                    myRectangle.setId("Player");
                                    myRectangle.setFill(new ImagePattern(new Image(getImage())));
                                }
                            }
                        }
                        break;
                    case "StraightLine":
                        myRectangle.setId("StraightLine");
                        myRectangle.setDirectionEnemy(getEnemyDirection());
                        myRectangle.setFill(new ImagePattern(new Image(getImage())));
                        break;
                    case "WallFollow":
                        myRectangle.setId("WallFollow");
                        myRectangle.setDirectionEnemy(getEnemyDirection());
                        myRectangle.setFill(new ImagePattern(new Image(getImage())));
                        break;

                    case "DumbTarget":
                        myRectangle.setId("DumbTarget");
                        myRectangle.setDirectionEnemy(getEnemyDirection());
                        myRectangle.setFill(new ImagePattern(new Image(getImage())));
                        break;
                    case "TokenDoor":
                        myRectangle.setId("TokenDoor");
                        myRectangle.setRequiredTokenDoor(Integer.parseInt(tokenInput()));
                        myRectangle.setFill(new ImagePattern(new Image(getImage())));
                        break;
                    default:
                        myRectangle.setId(data);
                        myRectangle.setFill(new ImagePattern(new Image(getImage())));
                        break;
                }

            }

        } else {
            if (myRectangle.getId().equals("Teleporter")) {
                firstTeleporter.setId("id");
                ((MyRectangle) firstTeleporter).setFill(Color.WHITE);
                secondTeleporter.setId("id");
                ((Rectangle) secondTeleporter).setFill(Color.WHITE);
                delete.setDisable(false);
            } else {
                myRectangle.setId("id");
                myRectangle.setFill(Color.WHITE);
                delete.setDisable(false);
            }
        }
    }

    /**
     * Method that reads which data has been selected in each comboBox and returns the
     * corresponding image related to that item, cell or character
     *
     * @return Data of the selected combo box
     */
    private String getImage() {
        String data = " ";
        if (!this.data.isEmpty()) {
            if (this.data.equals("RedKey")) {
                data = "images/redKey.png";
            } else if (this.data.equals("BlueKey")) {
                data = "images/blueKey.png";
            } else if (this.data.equals("token")) {
                data = "images/token.png";
            } else if (this.data.equals("GreenKey")) {
                data = "images/greenKey.png";
            } else if (this.data.equals("RainUmbrella")) {
                data = "images/umbrella.png";
            } else if (this.data.equals("WindJacket")) {
                data = "images/jacket.png";
            } else if (this.data.equals("SmartTarget")) {
                data = "images/enemySmart.png";
            } else if (this.data.equals("WallFollow")) {
                data = "images/enemyWall.png";
            } else if (this.data.equals("StraightLine")) {
                data = "images/enemyStraight.png";
            } else if (this.data.equals("DumbTarget")) {
                data = "images/enemyDumb.png";
            } else if (this.data.equals("Ice")) {
                data = "images/ice.png";
            } else if (this.data.equals("Wind")) {
                data = "images/wind.png";
            } else if (this.data.equals("Rain")) {
                data = "images/rain.png";
            } else if (this.data.equals("BlueDoor")) {
                data = "images/blueDoor.png";
            } else if (this.data.equals("GreenDoor")) {
                data = "images/greenDoor.png";
            } else if (this.data.equals("RedDoor")) {
                data = "images/redDoor.png";
            } else if (this.data.equals("Ground")) {
                data = "images/ground.png";
            } else if (this.data.equals("Teleporter")) {
                data = "images/teleporter.png";
            } else if (this.data.equals("Wall")) {
                data = "images/wall.png";
            } else if (this.data.equals("Token")) {
                data = "images/token.png";
            } else if (this.data.equals("Player")) {
                data = "images/player2Flip.png";
            } else if (this.data.equals("Goal")) {
                data = "images/target.png";
            } else if (this.data.equals("TokenDoor")) {
                data = "images/tokenDoor.png";
            }
        }

        return data;
    }

    /**
     * Checks the specific cell from the map file
     * @param c character
     * @param currentMyRectangle rectangle to draw picture
     */
    private void checkCell(char c, MyRectangle currentMyRectangle) {
        switch (c) {
            case '#':
                data = "Wall";
                currentMyRectangle.setId("Wall");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case ' ':
                data = "Ground";
                currentMyRectangle.setId("Ground");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 'F':
                data = "Wind";
                currentMyRectangle.setId("Wind");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 'W':
                data = "Rain";
                currentMyRectangle.setId("Rain");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 'R':
                data = "RedDoor";
                currentMyRectangle.setId("RedDoor");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 'G':
                data = "GreenDoor";
                currentMyRectangle.setId("GreenDoor");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
            case 'B':
                data = "BlueDoor";
                currentMyRectangle.setId("BlueDoor");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case '!':
                data = "Goal";
                currentMyRectangle.setId("Goal");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 'I':
                data = "Ice";
                currentMyRectangle.setId("Ice");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 'r':
                data = "RedKey";
                currentMyRectangle.setId("RedKey");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 'g':
                data = "GreenKey";
                currentMyRectangle.setId("GreenKey");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 'b':
                data = "BlueKey";
                currentMyRectangle.setId("BlueKey");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 'f':
                data = "WindJacket";
                currentMyRectangle.setId("WindJacket");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 'w':
                data = "RainUmbrella";
                currentMyRectangle.setId("RainUmbrella");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            case 't':
                data = "Token";
                currentMyRectangle.setId("Token");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                break;
            default:
                System.exit(-1);
        }
    }

    /**
     * Method to create the grid Pane with coordinates and colors
     *
     * @param x Coordinate x of the pane of Grids
     * @param y Coordinate y of the pane of Grids
     * @return The grid Pane with the shape of a rectangle
     */
    private Rectangle createElement(int x, int y) {
        MyRectangle rectangle = new MyRectangle();
        rectangle.setWidth(60);
        rectangle.setHeight(60);
        rectangle.setStroke(Color.BLACK);
        rectangle.setFill(Color.WHITE);
        rectangle.setX(x);
        rectangle.setY(y);
        rectangle.setId("id");
        return rectangle;
    }

    /**
     * This method read map file and create grid for editing existing map
     *
     * @param fileName name of the map file
     */
    private void readMap(String fileName) {
        Log log = new Log("ReadFile");
        File inputFile = new File(fileName);
        Scanner in = null;
        try {
            in = new Scanner(inputFile);
        } catch (FileNotFoundException e) {
            System.out.println("Cannot open " + fileName);
            System.exit(-1);
        }
        Scanner mapSizeLine = new Scanner(in.nextLine());
        mapSizeLine.useDelimiter(",");
        int maxX = mapSizeLine.nextInt();
        int maxY = mapSizeLine.nextInt();
        Scanner mapSpawnLine = new Scanner(in.nextLine());
        mapSpawnLine.useDelimiter(",");
        Cell[][] cells = new Cell[maxX][maxY];
        log.add("Map Size: " + maxX + "," + maxY);
        readLayout(in, maxX, maxY);
        readSpecials(cells, in);

        int spawnX = mapSpawnLine.nextInt();
        int spawnY = mapSpawnLine.nextInt();

        MyRectangle currentMyRectangle = new MyRectangle();

        for (Node n : gridPane.getChildren()) {
            if (((MyRectangle) n).getX() == spawnX && ((MyRectangle) n).getY() == spawnY) {
                currentMyRectangle = (MyRectangle) n;
                data = "Player";
                currentMyRectangle.setId("Player");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
            }
        }

    }

    /**
     * Reads the layout of the map and converts it into a cell array.
     *
     * @param in   Scanner containing file
     * @param maxX Size of the X axis
     * @param maxY Size of the Y axis
     */
    private void readLayout(Scanner in, int maxX, int maxY) {
        for (int y = 0; y < maxY; y++) {
            Scanner line = new Scanner(in.nextLine());
            char[] currentLine = line.nextLine().toCharArray();
            for (int x = 0; x < maxX; x++) {
                MyRectangle currentMyRectangle = new MyRectangle();
                for (Node n : gridPane.getChildren()) {
                    if (((MyRectangle) n).getX() == x && ((MyRectangle) n).getY() == y) {
                        currentMyRectangle = (MyRectangle) n;
                    }
                }
                char c = currentLine[x];
                checkCell(c, currentMyRectangle);


            }
        }
    }

    /**
     * Reads the special Cells/Items/Characters of the map and adds them to the Cell array.
     *
     * @param cells Cell array to read into
     * @param in    Scanner containing file
     */
    private void readSpecials(Cell[][] cells, Scanner in) {
        while (in.hasNextLine()) {
            MyRectangle currentMyRectangle = new MyRectangle();
            Scanner line = new Scanner(in.nextLine());
            line.useDelimiter(",");
            String token = line.next();
            int x;
            int y;
            int speed;
            String direction;
            x = line.nextInt();
            y = line.nextInt();
            for (Node n : gridPane.getChildren()) {
                if (((MyRectangle) n).getX() == x && ((MyRectangle) n).getY() == y) {
                    currentMyRectangle = (MyRectangle) n;
                }
            }
            if ("enemyStraight".equals(token)) {
                speed = line.nextInt();
                direction = line.next();
                data = "StraightLine";
                currentMyRectangle.setId("StraightLine");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                Direction dir = readDirection(direction);
                if (dir == Direction.RIGHT) {
                    currentMyRectangle.setDirectionEnemy("RIGHT");
                } else if (dir == Direction.LEFT) {
                    currentMyRectangle.setDirectionEnemy("LEFT");
                } else if (dir == Direction.UP) {
                    currentMyRectangle.setDirectionEnemy("UP");
                } else if (dir == Direction.DOWN) {
                    currentMyRectangle.setDirectionEnemy("DOWN");
                }
            } else if ("enemySmart".equals(token)) {
                speed = line.nextInt();
                direction = line.next();
                data = "SmartTarget";
                currentMyRectangle.setId("SmartTarget");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                Direction dir = readDirection(direction);
                if (dir == Direction.RIGHT) {
                    currentMyRectangle.setDirectionEnemy("RIGHT");
                } else if (dir == Direction.LEFT) {
                    currentMyRectangle.setDirectionEnemy("LEFT");
                } else if (dir == Direction.UP) {
                    currentMyRectangle.setDirectionEnemy("UP");
                } else if (dir == Direction.DOWN) {
                    currentMyRectangle.setDirectionEnemy("DOWN");
                }
            } else if ("enemyDumb".equals(token)) {
                speed = line.nextInt();
                direction = line.next();
                data = "DumbTarget";
                currentMyRectangle.setId("DumbTarget");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                Direction dir = readDirection(direction);
                if (dir == Direction.RIGHT) {
                    currentMyRectangle.setDirectionEnemy("RIGHT");
                } else if (dir == Direction.LEFT) {
                    currentMyRectangle.setDirectionEnemy("LEFT");
                } else if (dir == Direction.UP) {
                    currentMyRectangle.setDirectionEnemy("UP");
                } else if (dir == Direction.DOWN) {
                    currentMyRectangle.setDirectionEnemy("DOWN");
                }
            } else if ("enemyWall".equals(token)) {
                direction = line.next();
                data = "DumbTarget";
                currentMyRectangle.setId("DumbTarget");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
                Direction dir = readDirection(direction);
                if (dir == Direction.RIGHT) {
                    currentMyRectangle.setDirectionEnemy("RIGHT");
                } else if (dir == Direction.LEFT) {
                    currentMyRectangle.setDirectionEnemy("LEFT");
                } else if (dir == Direction.UP) {
                    currentMyRectangle.setDirectionEnemy("UP");
                } else if (dir == Direction.DOWN) {
                    currentMyRectangle.setDirectionEnemy("DOWN");
                }
            } else if ("tokenDoor".equals(token)) {
                int requirement = line.nextInt();
                data = "TokenDoor";
                currentMyRectangle.setId("TokenDoor");
                currentMyRectangle.setRequiredTokenDoor(requirement);
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
            } else if ("teleporter".equals(token)) {
                data = "Teleporter";
                currentMyRectangle.setId("Teleporter");
                currentMyRectangle.setFill(new ImagePattern(new Image(getImage())));
            } else {
            }
        }
    }

    /**
     * Reads direction and finds correct Enum type.
     *
     * @param dir direction
     * @return direction as an enumerated type
     */
    private Direction readDirection(String dir) {
        if ("LEFT".equals(dir)) {
            return Direction.LEFT;
        } else if ("RIGHT".equals(dir)) {
            return Direction.RIGHT;
        } else if ("UP".equals(dir)) {
            return Direction.UP;
        } else if ("DOWN".equals(dir)) {
            return Direction.DOWN;
        }
        return null;
    }


    /**
     * Method to set the different rows of the grid Pane
     *
     * @param newRows the rows of the Grid Pane
     */
    private void setRows(int newRows) {
        rowCount = newRows;
        gridPane.setPrefRows(rowCount);
        createElements();
    }

    /**
     * Method that draws the actual pane with all the grids and coordinates
     *
     * @param x Coordinate x in the pane of grids
     * @param y Coordinate y in the pane of grids
     */
    @FXML
    private void drawGrid(int x, int y) {
        gridPane.setVgap(1);
        gridPane.setHgap(1);
        setColumns(y);
        setRows(x);
        accessRectangle();

    }

    /**
     * Draw the map provided elements on the grid
     *
     * @param x    direction
     * @param y    direction
     * @param file path
     */
    private void drawCustomGrid(int x, int y, String file) {
        gridPane.setVgap(1);
        gridPane.setHgap(1);
        setColumns(y);
        setRows(x);
        readMap(file);
        accessRectangle();
    }

    /**
     * Method that will delete the element of the corresponding grid in the pane when we click the Delete Button
     * and the grid that we want to delete it from.
     *
     * @param event Event of the user clicking the button
     */
    @FXML
    public void deleteButton(ActionEvent event) {
        delete.setDisable(true);
    }

    /**
     * Method that will save the Pane of Grids when we click the Save Button
     *
     * @param event Event of the user clicking the button
     */
    @FXML
    public void saveButton(ActionEvent event) {
        FileCreatorGrids grids = new FileCreatorGrids();
        grids.createFileFromString(grids.writeString(gridPane, columnCount, rowCount, border), mapInput() + ".map");
    }


    /**
     * Method that checks how many ids of the same type are being used and returns a counter specifying the number
     *
     * @param name Name of the elements that are being used that will have the same ids
     * @return The number of the same ids, meaning the number of the same elements
     */
    private int countID(String name) {
        int count = 0;
        for (Node n : gridPane.getChildren()) {
            if (n instanceof Rectangle) {
                if (n.getId().equals(name)) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * Method to set all the three combo Boxes to true for a future use with teleporter and player.
     */
    private void disableBoxes() {
        comboBoxItems.setDisable(true);
        comboBoxEnemies.setDisable(true);
        comboBoxCells.setDisable(true);
    }

    /**
     * Method to set all the three combo Boxes to false for a future use with teleporter and player.
     */
    private void enableBoxes() {
        comboBoxItems.setDisable(false);
        comboBoxEnemies.setDisable(false);
        comboBoxCells.setDisable(false);
    }

    /**
     * Opens the stage of enemy controller and returns the direction of enemy
     *
     * @return direction enemy
     */
    public String getEnemyDirection() {
        String data = "";
        try {
            EnemyDirectionController contr = new EnemyDirectionController();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("chooseDirection.fxml"));

            Stage stage1 = new Stage();
            stage1.setScene(new Scene(loader.load(), 600, 400));
            stage1.showAndWait();
            if (contr.getComplete()) {
                data = contr.getValue();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;


    }

    /**
     * Method to change scenes from the one where you edit the level to the one where you choose
     * the number of door tokens that you want.
     *
     * @return number of tokens
     */
    private String tokenInput() {
        String data = "";
        try {
            ContinueButtonsController contr = new ContinueButtonsController();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ChooseTokens.fxml"));

            Stage stage1 = new Stage();
            stage1.setScene(new Scene(loader.load(), 600, 400));
            stage1.showAndWait();
            if (contr.getComplete()) {
                data = contr.getValueToken();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * Method to change scenes from the one where you edit the level to the one where you choose
     * the name of the map for the one that you have created.
     *
     * @return name of the map
     */
    private String mapInput() {
        String data = "";
        try {
            ContinueButtonsController contr = new ContinueButtonsController();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ChooseNameMap.fxml"));

            Stage stage1 = new Stage();
            stage1.setScene(new Scene(loader.load(), 600, 400));
            stage1.showAndWait();
            if (contr.getComplete()) {
                data = contr.getValueMap();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}
