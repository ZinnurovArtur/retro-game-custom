package menu;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.effect.*;
import javafx.scene.image.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Skinchangercontroller {

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private Button save;

    @FXML
    private Button cancel;


    @FXML
    private ImageView imageView;


    public void changePreview(ActionEvent event) throws IOException {

        Image newimageflip = reColor(imageView.getImage(),
                colorPicker.valueProperty().getValue(), colorPicker.getValue());
        imageView.setImage(newimageflip);


    }

    private Image reColor(Image inputImage, Color oldColor, Color newColor) {
        System.out.println(oldColor + " " + newColor);
        int width = (int) inputImage.getWidth();
        int height = (int) inputImage.getHeight();
        WritableImage outputImage = new WritableImage(width, height);
        System.out.println(width + " " + height);
        PixelReader reader = inputImage.getPixelReader();
        PixelWriter writer = outputImage.getPixelWriter();
        int newblue = (int) Math.round(newColor.getBlue() * 255);
        int newgreen = (int) Math.round(newColor.getGreen() * 255);
        int newRed = (int) Math.round(newColor.getRed() * 255);


        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int argb = reader.getArgb(x, y);
                int a = (argb >> 24) & 0xFF;
                int r = (argb >> 16) & 0xFF;
                int g = (argb >> 8) & 0xFF;
                int b = argb & 0xFF;

                r = newRed;
                g = newgreen;
                b = newblue;


                argb = (a << 24) | (r << 16) | (g << 8) | b;
                writer.setArgb(x, y, argb);
            }
        }

        return outputImage;

    }

    private void saveImage(Image secondimage) {
        try {

            BufferedImage bi = imageToBufferedImage(imageView.getImage());
            File outputfile = new File("src/images/player2skin.png");
            ImageIO.write(bi, "png", outputfile);

            BufferedImage bi2 = imageToBufferedImage(secondimage);
            File outputfile2 = new File("src/images/player2Flipskin.png");
            ImageIO.write(bi2, "png", outputfile2);

        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    private BufferedImage imageToBufferedImage(Image img) {
        BufferedImage bimage = SwingFXUtils.fromFXImage(img, null);
        return bimage;
    }


    public void saveButton(ActionEvent event) throws IOException{

        Image newImageflip2 = reColor(new Image("images/player2Flipskin.png"), colorPicker.valueProperty().getValue(),
                colorPicker.getValue());
        saveImage(newImageflip2);
        Platform.exit();
    }

    public void cancelButton(ActionEvent event) throws IOException{
        Platform.exit();
    }
}

