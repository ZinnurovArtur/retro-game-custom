package menu;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


/**
 * This class will control the continue buttons of the ChooseNameMap and Choose Tokens
 * @author Arthur Zinnurov
 * @author Suad Ahmadieh Mena
 * @version 1.0
 */
public class ContinueButtonsController {
    @FXML
    private TextField numberOfTokens;
    @FXML
    private TextField nameOfTheMap;
    @FXML
    private Button continueMaps;

    static  String valueToken;
    static  String valueMap;



    /**
     * Method to exit the scene from the one where you have to select the name of the map
     * to the one where you will edit the level.
     * @param event Event of the user clicking the button
     */
    @FXML
    public void continueMapsButton(ActionEvent event) {
        if(!(takeDataMap().isEmpty())){
            valueMap = takeDataMap();
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            getComplete();
            window.close();
        }else {
            nameOfTheMap.setText("");
            takeDataMap();
        }

    }

    /**
     * Method to change scenes from play game to start creating or reloading profiles when you click the Play Button.
     * @param event Event of the user clicking the button
     */
    @FXML
    public void continueTokensButton(ActionEvent event){
        if(!(takeDataTokens().isEmpty())){
            valueToken = takeDataTokens();
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            getComplete();
            window.close();
        }else {
            numberOfTokens.setText("");
            takeDataTokens();
        }

    }

    /**
     * Check that Stage has finished process
     * @return boolean
     */
    public boolean getComplete(){
        return true;

    }

    /**
     * Return token value in order to pass to the first Controller
     * @return String value
     */
    public String getValueToken(){
        return valueToken;
    }

    /**
     * Return map name in order to pass to the first Controller
     * @return String name
     */
    public String getValueMap(){
        return valueMap;
    }

    /**
     * Validation of textfield
     * @return validated string
     */
    private String takeDataTokens(){
        String value = numberOfTokens.getText();
        if (!(isNumeric(value))){
            value = "";
            numberOfTokens.setPromptText("Integers!");
        }else {
            value = numberOfTokens.getText().toUpperCase();
        }
        return value;
    }

    /**
     * Return string name of the map with validation
     * @return name of the map
     */
    private String takeDataMap(){
        String value = nameOfTheMap.getText();
        if (value.isEmpty()){
            value = "";
            nameOfTheMap.setPromptText("Name!");
        }else {
            value = nameOfTheMap.getText();
        }
        return value;


    }


    /**
     * Check that textfield has numeric value
     * @param string from textfield
     * @return boolean
     */
    private boolean isNumeric(String string){
        boolean numeric = true;
        try{
            Double num = Double.parseDouble(string);
        }catch (NumberFormatException e){
            numeric = false;

        }
        return numeric;
    }





}
