package menu;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LevelEditorController implements Initializable {
    private final PlayButton buttonSound = new PlayButton(60);
    static String typeChoice = "";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    /**
     * Call the system choice button
     * @param event any events
     * @throws IOException
     */
    @FXML
    public void SystemMapsButtom(ActionEvent event) throws IOException {
        buttonSound.playButton();
        typeChoice = "system";
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("ChooseLevel.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        // This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }

    /**
     * Call custom button choice
     * @param event
     * @throws IOException
     */
    @FXML
    public void CustomMapsButtom(ActionEvent event) throws IOException {
        buttonSound.playButton();
        typeChoice = "custom_maps";
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("ChooseLevel.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        // This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }

    /**
     * Call level editor button choice
     * @param event
     * @throws IOException
     */
    @FXML
    public void GotoLevelEditorButtom(ActionEvent event) throws IOException {
        buttonSound.playButton();
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("CreateNewMapOrLoadCurrentMap.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        // This line gets the Stage information
       // Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Stage window = new Stage();
        window.setX(600);
        window.setScene(tableViewScene);
        window.show();
    }

    /**
     * Return system or custom  choice for level editor scene
     * @return String which type user has choosen
     */
    public String getTypeChoice(){
        return typeChoice;
    }

    @FXML
    public void goBack(ActionEvent event) throws IOException{
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("ReloadGame.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }
}

