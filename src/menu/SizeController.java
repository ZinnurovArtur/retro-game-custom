package menu;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller to initialise the size of the Grid
 * @author  Arthur Zinnurov
 * @author Suad Ahmadieh Mena
 */

public class SizeController implements Initializable {

    @FXML
    private TextField x;
    @FXML
    private TextField y;
    @FXML
    private Button applyButton;

    static int xValue;
    static int yValue;


    /**
     * Read the line of x textfield with validation
     * @return String
     */
    @FXML
    public String textFieldX() {
        String xcoord = x.getText();
        if(!isNumeric(xcoord)){
            xcoord = "";
            x.setPromptText("Put values!");
        }else {
            xcoord = x.getText();
        }
        return xcoord;
    }

    /**
     * Read the line of y textfield with validation
     * @return String
     */
    @FXML
    public String textFieldY() {
        String ycoord = y.getText();
        if(!isNumeric(ycoord)){
            ycoord = "";
            y.setPromptText("Put values!");
        }else {
            ycoord = y.getText();
        }
        return ycoord;
    }

    /**
     * Button to move to the next scene
     * @param event
     * @throws IOException
     */
    @FXML
    public void applyButtonact(ActionEvent event) throws IOException {
        System.out.println("ok");


        if (!(textFieldX().isEmpty()) && !(textFieldY().isEmpty())) {
            xValue = Integer.parseInt(textFieldX());
            yValue = Integer.parseInt(textFieldY());
            Parent tableViewParent = FXMLLoader.load(getClass().getResource("CustomMap.fxml"));
            Scene tableViewScene = new Scene(tableViewParent);

            Stage window2 = (Stage) ((Node) event.getSource()).getScene().getWindow();

            window2.setScene(tableViewScene);
            window2.setWidth(600);
            window2.setHeight(600);
            window2.show();
        }else {
            x.setText("");
            y.setText("");
            textFieldX();
            textFieldY();
        }



    }

    /**
     * Return the value from x textfield
     * @return int xvalue
     */
    public int getxValue() {
        return xValue;

    }

    /**
     * Return the value from the y textfield
     * @return  int yValue
     */
    public int getyValue() {
        return yValue;
    }

    /**
     * Check that our coordinates actually numbers
     * @param string
     * @return boolean
     */
    private boolean isNumeric(String string){
        boolean numeric = true;
        try{
            Double num = Double.parseDouble(string);
        }catch (NumberFormatException e){
            numeric = false;

        }
        return numeric;
    }

    /**
     * Actual initialise of this stage
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
