package menu;

import javafx.scene.Node;
import javafx.scene.layout.TilePane;
import javafx.scene.shape.Rectangle;

import java.io.*;
/**
 * This class is meant to save the edited map as a file to follow the same format of the actual game
 * @author Suad Ahmadieh Mena
 * @author Arthur Zinnurov
 * @author Piotr Obara
 */
public class FileCreatorGrids {
    String s = "";
    PrintWriter printWriter;

    /**
     * Method for converting the level editor map in String
     * @param myGrids The current grid of panes
     * @param nCols The columns of the grid of panes
     * @param nRows The rows of the grid of panes
     * @return string Map
     */
    public String writeString(TilePane myGrids, int nCols, int nRows, boolean border) {
        Node[][] mapData = new Node[nCols][nRows];
        if(border) {
            mapData = new Node[nCols + 2][nRows + 2];
            for (Node n : myGrids.getChildren()) {
                if (n instanceof Rectangle) {
                    int x = (int) ((Rectangle) n).getX();
                    int y = (int) ((Rectangle) n).getY();
                    mapData[x + 1][y + 1] = n;
                }
            }
            for (int i = 0; i < mapData.length; i++) {
                for (int j = 0; j < mapData[i].length; j++) {
                    Rectangle rectangle = new Rectangle();
                    if (mapData[i][j] == null) {
                        mapData[i][j] = rectangle;
                        rectangle.setId("Wall");
                    }
                }
            }
            s += (nRows + 2) + "," + (nCols + 2) + "\n";
            s += getPlayerLocation(mapData) + "\n";
        }else {
            for (Node n : myGrids.getChildren()) {
                if (n instanceof Rectangle) {
                    int x = (int) ((Rectangle) n).getX();
                    int y = (int) ((Rectangle) n).getY();
                    mapData[x][y] = n;
                }
            }
            s += (nRows) + "," + (nCols) + "\n";
            s += getPlayerLocation(mapData) + "\n";
        }


        System.out.println(myGrids.getChildren());



        for (int i = 0; i < mapData.length; i++) {
            for (int j = 0; j < mapData[i].length; j++) {

                if (mapData[i][j].getId().equals("Wall")) {
                    s += "#";
                } else if (mapData[i][j].getId().equals("Ground")) {
                    s += " ";
                } else if (mapData[i][j].getId().equals("Wind")) {
                    s += "F";
                } else if (mapData[i][j].getId().equals("Rain")) {
                    s += "W";
                } else if (mapData[i][j].getId().equals("RedDoor")) {
                    s += "R";
                } else if (mapData[i][j].getId().equals("GreenDoor")) {
                    s += "G";
                } else if (mapData[i][j].getId().equals("BlueDoor")) {
                    s += "B";
                } else if (mapData[i][j].getId().equals("Goal")) {
                    s += "!";
                } else if (mapData[i][j].getId().equals("Ice")) {
                    s += "I";
                } else  if (mapData[i][j].getId().equals("Token")) {
                    s += "t";
                } else if (mapData[i][j].getId().equals("RedKey")) {
                    s += "r";
                } else if (mapData[i][j].getId().equals("GreenKey")) {
                    s += "g";
                } else if (mapData[i][j].getId().equals("BlueKey")) {
                    s += "b";
                } else if (mapData[i][j].getId().equals("WindJacket")) {
                    s += "f";
                } else if (mapData[i][j].getId().equals("RainUmbrella")) {
                    s += "w";
                } else if (mapData[i][j].getId().equals("Player")) {
                    s += " ";
                } else if (mapData[i][j].getId().equals("StraightLine")) {
                    s += " ";
                } else if (mapData[i][j].getId().equals("WallFollow")) {
                    s += " ";
                } else if (mapData[i][j].getId().equals("SmartTarget")) {
                    s += " ";
                } else if (mapData[i][j].getId().equals("DumbTarget")) {
                    s += " ";
                } else if (mapData[i][j].getId().equals("TokenDoor")) {
                    s += " ";
                } else if (mapData[i][j].getId().equals("Teleporter")) {
                    s += " ";
                } else if (mapData[i][j].getId().equals("id")) {
                    s += " ";
                }
            }
            s += "\n";
        }


        s += findTeleporter(mapData);
        s += findEnemyStraight(mapData);
        s += findEnemyWall(mapData);
        s += findEnemySmart(mapData);
        s += findEnemyDumb(mapData);
        s += findTokenDoor(mapData);

        System.out.println(s);
        return s;
    }



    /**
     * Method to get where the player is in the pane of grids due to we only can have one player in the grid.
     * @param mapData The coordinates in the pane where the player is located.
     * @return the position of the player on the pane as a String
     */
    public String getPlayerLocation(Node[][] mapData) {

        String s = "";
        for (int i = 0; i < mapData.length; i++) {
            for (int j = 0; j < mapData[i].length; j++) {
                if (mapData[i][j].getId().equals("Player")) {
                    s += j + "," + i;
                }
            }
        }

        return s;
    }

    /**
     *Method to get where the teleporters are in the pane of grids due to we only can have two teleporters in the
     * gri which are going to be connected.
     * @param mapData The coordinates in the pane where the teleporters are located.
     * @return the position of the teleporters in the pane as a String
     */

    public String findTeleporter(Node[][] mapData) {
        String su = "teleporter";
        for (int i = 0; i < mapData.length; i++) {
            for (int j = 0; j < mapData[i].length; j++) {
                if (mapData[i][j].getId().equals("Teleporter")) {
                    su += "," + j + "," + i;
                }
            }
        }
        if (!su.equals("teleporter")){
            su += "\n";
        } else {
            su = "";
        }
        return su;
    }

    /**
     *Method to get where the wall enemies are in the pane of grids.
     * @param mapData The coordinates in the pane where the wall enemies are located.
     * @return the position of the wall enemies in the pane as a String
     */
    public String findEnemyWall(Node[][] mapData) {
        String su = "";
        for (int i = 0; i < mapData.length; i++) {
            for (int j = 0; j < mapData[i].length; j++) {
                if (mapData[i][j].getId().equals("WallFollow")) {
                    su += "enemyWall," + j + "," + i + "," + String.valueOf(1) + "," + ((MyRectangle) mapData[i][j]).getDirectionEnemy()+ "\n";
                }
            }
        }
        return su;
    }

    /**
     * Method to get where the smart enemies are in the pane of grids
     * @param mapData The coordinates in the pane where the smart enemies are located.
     * @return the position of the smart enemies in the pane as a String
     */
    public String findEnemySmart(Node[][] mapData) {
        String su = "";
        for (int i = 0; i < mapData.length; i++) {
            for (int j = 0; j < mapData[i].length; j++) {
                if (mapData[i][j].getId().equals("SmartTarget")) {
                    su += "enemySmart," + j + "," + i + "," + String.valueOf(1) + "," + "RIGHT" + "\n";
                }
            }

        }
        return su;
    }

    /**
     *Method to get where the straight enemy are in the pane of grids
     * @param mapData The coordinates in the pane where the straight enemies are located.
     * @return the position of the straight enemies in the pane as a String
     */
    public String findEnemyStraight(Node[][] mapData) {
        String su = "";
        for (int i = 0; i < mapData.length; i++) {
            for (int j = 0; j < mapData[i].length; j++) {
                if (mapData[i][j].getId().equals("StraightLine")) {
                    su += "enemyStraight," + String.valueOf(i) + "," + String.valueOf(j) + "," + String.valueOf(1) + "," + ((MyRectangle) mapData[i][j]).getDirectionEnemy() + "\n";
                }
            }

        }
        return su;
    }

    /**
     *Method to get where the dumb enemies are in the pane of grids
     * @param mapData The coordinates in the pane where the dumb enemies are located.
     * @return the position of the dumb enemies in the pane as a String
     */
    public String findEnemyDumb(Node[][] mapData) {
        String su = "";
        for (int i = 0; i < mapData.length; i++) {
            for (int j = 0; j < mapData[i].length; j++) {
                if (mapData[i][j].getId().equals("DumbTarget")) {
                    su += "enemyDumb," + j + "," + i + "," + String.valueOf(1) + "," + ((MyRectangle) mapData[i][j]).getDirectionEnemy() + "\n";

                }
            }
        }
        return su;
    }

    /**
     * Method to get where the door tokens are is in the pane of grids
     * @param mapData The coordinates in the pane where the door tokens are located.
     * @return the position of the door tokens in the pane as a String
     */
    public String findTokenDoor(Node[][] mapData) {
        String su = "";
        for (int i = 0; i < mapData.length; i++) {
            for (int j = 0; j < mapData[i].length; j++) {
                if (mapData[i][j].getId().equals("TokenDoor")) {
                    su += "tokenDoor," + j + "," + i + "," + ((MyRectangle) mapData[i][j]).getRequiredTokenDoor() + "\n";
                }
            }
        }
        return su;
    }

    /**
     * This method is going to take the map as a String that we have created with all the including elements
     * and is going to converted as a file
     * @param s The map that we have created as a String
     * @param fileName the file that we are going to convert it to as a String
     */
    public void createFileFromString(String s, String fileName) {
        try {

            printWriter = new PrintWriter(System.getProperty("user.dir")+
                    File.separator +"src"+File.separator + "custom_maps" + File.separator + fileName);
            printWriter.print(s);
            printWriter.close();
        } catch (IOException e){
            System.exit(-1);
        }

    }


}
