package menu;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class controller to open pop up window to put direction options of enemies
 *
 * @author Arthur Zinnurov
 * @version 1.0
 */
public class EnemyDirectionController implements Initializable {
    @FXML
    private Button continueBut;
    @FXML
    private TextField textField;
    static String value;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Action button to close window when user has put values
     *
     * @param event
     */
    @FXML
    private void continueAction(ActionEvent event) {
        if (!(takeData().isEmpty())) {
            value = takeData();
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            getComplete();
            window.close();

        } else {
            textField.setText("");
            takeData();
        }

    }

    /**
     * Method to validate and take string value of user input
     *
     * @return String direction
     */
    private String takeData() {
        String value = textField.getText().toUpperCase();
        if (!(value.equals("DOWN") || value.equals("UP") || value.equals("LEFT") || value.equals("RIGHT"))) {
            value = "";
            textField.setPromptText("Choose options");
        } else {
            value = textField.getText().toUpperCase();
        }
        return value;
    }

    /**
     * This method to return value to the first Controller
     *
     * @return value
     */
    public String getValue() {
        return value;
    }

    /**
     * Checks that Stage has done the process
     *
     * @return boolean
     */
    public boolean getComplete() {
        return true;

    }


}


