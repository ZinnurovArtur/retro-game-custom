package menu;

import com.sun.webkit.WebPage;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import javafx.scene.image.ImageView;

import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Class represents implementation of Audio player feature
 * @author Arthur Zinnurov
 * @version 1.10
 */



public class YoutubeVideoPlayer extends Application{


    /**
     * Start the stage ov youtube player
     * @param stage
     * @throws Exception
     */
    @Override
        public void start(Stage stage) throws Exception {




           Node node = stage.getScene().getRoot().getChildrenUnmodifiable().get(0);
           WebView webView = ((WebView)node);
           stage.setTitle("Youtube Music");
           stage.show();


            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    System.out.println("Stage is closing");
                    webView.getEngine().load(null);



                }
            });

        }



    public static void main(String[] args) {
            launch(args);
        }
}
